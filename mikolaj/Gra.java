package mikolajki;

import javax.swing.*;
import java.awt.*;
import java.util.Random;


//co z porzuconymi prezentami? jak dziecko na nie wejdzie losowo, to je bierze

public class Gra extends JPanel{
    private JLabel prezenty_info;
    private SiatkaGry siatka;
    private Mikolaj mikolaj;
    private Dzieciak[] dzieciaki;
    private int obdarowane_dzieciaki;
    private boolean czy_aktywna;

    private int rozmiar = 15;
    private int szer = 40;
    private int ile_dzieciakow = 4;

    public Gra(){
        int szerokosc = rozmiar*szer;
        int wysokosc_info = 100;
        int wysokosc = szerokosc + wysokosc_info;
        setSize(szerokosc, wysokosc);
        setPreferredSize(new Dimension(szerokosc, wysokosc));
        setLayout(new BoxLayout(this, BoxLayout.PAGE_AXIS));

        obdarowane_dzieciaki = 0;
        czy_aktywna = true;

        siatka = new SiatkaGry(rozmiar, this);
        prezenty_info = new JLabel("zostało " + ile_dzieciakow + " prezentów");
        prezenty_info.setPreferredSize(new Dimension(szerokosc, wysokosc_info));

        add(siatka);
        add(prezenty_info);

        mikolaj = new Mikolaj(ile_dzieciakow, siatka);
        dzieciaki = new Dzieciak[ile_dzieciakow];
        Pair [] wsp_startowe = losujWspolrzedneStartowe();
        for(int i=0; i<ile_dzieciakow; i++)
            dzieciaki[i] = new Dzieciak(wsp_startowe[i].w, wsp_startowe[i].k, siatka);
        for(int i=0; i<ile_dzieciakow; i++)
            dzieciaki[i].start();
    }
    public void przesunMikolaja(int w_przes, int k_przes){
        if (czy_aktywna) {
            int nowy_kol = (mikolaj.getKol() + k_przes + rozmiar) % rozmiar; //dodawanie rozmiaru, żeby było nieujemnie
            int nowy_wiersz = (mikolaj.getWiersz() + w_przes + rozmiar) % rozmiar;
            if (siatka.czyMoznaUstawicMikolaja(nowy_wiersz, nowy_kol)) {
                siatka.przesunMikolaja(mikolaj.getWiersz(), mikolaj.getKol(), nowy_wiersz, nowy_kol);
                mikolaj.ustawSie(nowy_wiersz, nowy_kol);
            }
        }
    }
    public void mikolajZostawiaPrezent(){
        if (czy_aktywna) {
            mikolaj.zostawPrezent();
            aktualizuj_info();
        }
    }
    private void aktualizuj_info(){
        prezenty_info.setText(("zostało " + mikolaj.ileZostaloPrezentow() + " prezentów"));
    }
    public Pair wspolrzedneMikolaja(){
        return new Pair(mikolaj.getWiersz(), mikolaj.getKol());
    }
    public synchronized void przegrana(){
        System.out.println("przegrana!");
        prezenty_info.setText("PRZEGRANA!");
        czy_aktywna = false;
        for(int i=0; i<ile_dzieciakow; i++) //z jakiegoś powodu nie zatrzymuje
            dzieciaki[i].stop();
        System.out.println(czy_aktywna);
        /*remove(siatka);
        remove(prezenty_info);
        add(new JLabel("PRZEGRANA!"));
        repaint();
        */
    }
    public synchronized void wygrana(){
        System.out.println("wygrana!");
        prezenty_info.setText("WYGRANA!");
        czy_aktywna = false;

    }
    public synchronized void sprawdzCzyWygrana(){
        if (obdarowane_dzieciaki == ile_dzieciakow)
            wygrana();
    }
    public void zliczDostarczonyPrezent(){
        obdarowane_dzieciaki++;
    }
    public boolean czyAktywna(){
        return czy_aktywna;
    }
    Pair [] losujWspolrzedneStartowe(){
        Pair [] wynik = new Pair [ile_dzieciakow];
        Random rand = new Random();
        for(int i=0; i<ile_dzieciakow; i++) { //losowanie tak, żeby na początku dzieciaki były nie za blisko (0, 0)
            boolean czy_nowe_wsp = true;
            int w_los, k_los;
            do{
                w_los = rand.nextInt(rozmiar * 2/3) + rozmiar/3;
                k_los = rand.nextInt(rozmiar * 2/3) + rozmiar/3;
                //sprawdzanie, czy się nie powtórzyło, niefektywne, ale dzieciaków jest mało
                for(int j=0; j<i; j++){
                    if (w_los == wynik[j].w && k_los == wynik[j].k){
                        czy_nowe_wsp = false;
                        break;
                    }
                }
            } while(!czy_nowe_wsp);
            wynik[i] = new Pair(w_los, k_los);
        }
        return wynik;
    }
}

class Pair{
    public int w, k;
    public Pair(int ww, int kk){
        w = ww;
        k = kk;
    }
}