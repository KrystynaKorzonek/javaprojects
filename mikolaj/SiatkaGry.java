package mikolajki;


import javax.swing.*;
import java.awt.*;


class SiatkaGry extends JPanel {
    private static int w_przesun [] = {1, -1, 0, 0};
    private static int k_przesun [] = {0, 0, 1, -1};
    private Pole[][] pola;
    private Gra gra;
    private int rozmiar;
    public SiatkaGry(int roz, Gra gr){
        rozmiar = roz;
        gra = gr;
        pola = new Pole[rozmiar][rozmiar];
        setLayout(new GridLayout(rozmiar, rozmiar));
        for(int x=0; x<rozmiar; x++)
            for(int y=0; y<rozmiar; y++){
                pola[x][y] = new Pole();
                add(pola[x][y]);
            }
    }
    public void przesunDzieciaka(Dzieciak d, int w_pocz, int k_pocz, int w_kon, int k_kon){
        pola[w_pocz][k_pocz].zabierzDzieciaka();
        pola[w_kon][k_kon].ustawDzieciaka(d);
        if (czySasiadujeZMikolajem(w_kon, k_kon))
            gra.przegrana();
    }
    private boolean czySasiadujeZMikolajem(int w, int k){
        Pair wsp_mik = gra.wspolrzedneMikolaja();
        int w_mik = wsp_mik.w;
        int k_mik = wsp_mik.k;
        for(int i=0; i<4; i++){
            int w_test = (w + w_przesun[i] + rozmiar) % rozmiar;
            int k_test = (k + k_przesun[i] + rozmiar) % rozmiar;
            if (w_mik == w_test && k_mik == k_test)
                return true;
        }
        return false;
    }
    private boolean czySasiadujeZAktywnymDzieciakiem(int w, int k){
        for(int i=0; i<4; i++) {
            int w_test = (w + w_przesun[i] + rozmiar) % rozmiar;
            int k_test = (k + k_przesun[i] + rozmiar ) % rozmiar;
            if(pola[w_test][k_test].czyJestDzieciak()){
                Dzieciak d = pola[w_test][k_test].getDzieciak();
                if (d.czyAktywny())
                    return true;
            }
        }
        return false;
    }
    public void wstawMikolaja(int w, int k){
        pola[w][k].ustawMikolaja();
    }
    public boolean czyMoznaUstawicMikolaja(int w, int k){
        return !(pola[w][k].czyJestDzieciak());
    }
    public boolean czyMoznaUstawicDzieciaka(int w, int k){
        if (pola[w][k].czyJestDzieciak())
            return false;
        Pair wsp_mik = gra.wspolrzedneMikolaja();
        if (wsp_mik.w == w && wsp_mik.k == k)
            return false;
        return true;
    }
    public void przesunMikolaja(int w_pocz, int k_pocz, int w_kon, int k_kon){
        pola[w_pocz][k_pocz].zabierzMikolaja();
        pola[w_kon][k_kon].ustawMikolaja();
        if (czySasiadujeZAktywnymDzieciakiem(w_kon, k_kon))
            gra.przegrana();
    }
    public void polozPrezent(int w, int k){
        pola[w][k].polozPrezent();
    }
    public boolean czyPrezent(int w, int k){
        return pola[w][k].czyJestPrezent();
    }
    public int getRozmiar(){
        return rozmiar;
    }
    public boolean czyObokJestMikolaj(int w, int k){
        Pair wsp_mikolaja = gra.wspolrzedneMikolaja();
        int w_mik = wsp_mikolaja.w;
        int k_mik = wsp_mikolaja.k;
        for(int i=0; i<4; i++){
            int w_test = w + w_przesun[i];
            int k_test = k + k_przesun[i];
            w_test = (w_test + rozmiar) % rozmiar;
            k_test = (k_test + rozmiar) % rozmiar;
            if (w_test == w_mik && k_test == k_mik)
                return true;
        }
        return false;
    }
    public Pair gdzieObokJestPrezent(int w, int k){
        for(int i=0; i<4; i++){
            int w_test = w + w_przesun[i];
            int k_test = k + k_przesun[i];
            w_test = (w_test + rozmiar) % rozmiar;
            k_test = (k_test + rozmiar) % rozmiar;
            if (pola[w_test][k_test].czyJestPrezent())
                return new Pair(w_test, k_test);
        }
        return null;
    }
    public Pair gdzieJestMikolaj(){
        return gra.wspolrzedneMikolaja();
    }
    public void zasygnalizujPrzegrana(){
        gra.przegrana();
    }
    public void sprawdzCzyWygrana(){
        gra.sprawdzCzyWygrana();
    }
    public void zliczDostarczonyPrezent(){
        gra.zliczDostarczonyPrezent();
    }
    public boolean czyGraAktywna(){
        return gra.czyAktywna();
    }

}
