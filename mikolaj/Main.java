package mikolajki;

import javax.swing.*;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;


class OknoGry extends JFrame {
    private Gra gra;
    public OknoGry() {
        super("Mikołaj i dzieciaki");
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        gra = new Gra();
        add(gra);
        pack();
        setVisible(true);
        setResizable(false);
        addKeyListener(new KeyListener() {
            @Override
            public void keyTyped(KeyEvent keyEvent) {}
            @Override
            public void keyPressed(KeyEvent e) {
                int kod = e.getKeyCode();
                if (kod == KeyEvent.VK_UP)
                    gra.przesunMikolaja(-1, 0);
                if (kod == KeyEvent.VK_DOWN)
                    gra.przesunMikolaja(1, 0);
                if (kod == KeyEvent.VK_LEFT)
                    gra.przesunMikolaja(0, -1);
                if (kod == KeyEvent.VK_RIGHT)
                    gra.przesunMikolaja(0, 1);
                if (kod == KeyEvent.VK_SHIFT)
                    gra.mikolajZostawiaPrezent();
            }
            @Override
            public void keyReleased(KeyEvent keyEvent) {}
        });
    }
}
public class Main {

    public static void main(String[] args){
	    new OknoGry();


    }
}
