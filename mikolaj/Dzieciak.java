package mikolajki;

import java.awt.*;
import java.util.Random;

public class Dzieciak extends Thread{
    private static Random losowacz = new Random();
    private static int w_przesun [] = {1, -1, 0, 0};
    private static int k_przesun [] = {0, 0, 1, -1};

    private int w, k;
    private SiatkaGry siatka; //???
    private int rozmiar;
    private boolean czy_spi;
    private boolean czy_ma_prezent;
    public Dzieciak(int w_pocz, int k_pocz, SiatkaGry siatka){
        w = w_pocz;
        k = k_pocz;
        this.siatka = siatka;
        rozmiar = siatka.getRozmiar();
        czy_spi = false;
        czy_ma_prezent = false;
    }
    public Color kolorDzieciaka(){
        if (czy_ma_prezent)
            return Color.GREEN;
        if(czy_spi)
            return Color.BLUE;
        return Color.RED;
    }
    private void przesunSie(int w_przes, int k_przes){
        int nowy_w = (w + w_przes + rozmiar) % rozmiar; //dodawanie rozmiaru, żeby było nieujemnie
        int nowy_k = (k + k_przes + rozmiar) % rozmiar;
        synchronized(siatka){
            if (!czy_ma_prezent && siatka.czyMoznaUstawicDzieciaka(nowy_w, nowy_k)) {
                siatka.przesunDzieciaka(this, w, k, nowy_w, nowy_k);
                w = nowy_w;
                k = nowy_k;
                if (siatka.czyPrezent(w, k)){
                    czy_ma_prezent = true;
                }
                siatka.repaint();
            }
        }
        try {
            sleep(420);
        }
        catch(InterruptedException e){}
    }
    private static int sgn(int x){
        if (x<0)
            return -1;
        else return 1;
    }
    private void idzDoMikolajaJesliBlisko(){
        Pair wsp_mik = siatka.gdzieJestMikolaj();
        int w_mik = wsp_mik.w;
        int k_mik = wsp_mik.k;
        int dist_w = Math.abs(w_mik - w);
        int dist_k = Math.abs(k_mik - k);
        if (dist_w <= 2 && dist_k <= 2){
            int do_przejscia_w = w_mik - w;
            int do_przejscia_k = k_mik - k;
            int krok_w = sgn(do_przejscia_w);
            int krok_k = sgn(do_przejscia_k);
            //dodatkowe sleepy, bo inaczej nie dało się uciec po namierzeniu
            for(int i=0; i<dist_w; i++) {
                przesunSie(krok_w, 0);
                try {
                    sleep(420);
                }
                catch(InterruptedException e){}
            }
            for(int i=0; i<dist_k; i++) {
                przesunSie(0, krok_k);
                try {
                    sleep(420);
                }
                catch(InterruptedException e){}
            }

        }
    }
    private void idzDoPrezentu(int w_prez, int k_prez){
        siatka.przesunDzieciaka(this, w, k, w_prez, k_prez);
        siatka.repaint();
        w = w_prez;
        k = k_prez;
    }
    public void run() {
        while(!czy_ma_prezent && siatka.czyGraAktywna()){
            if (siatka.czyObokJestMikolaj(w, k))
                siatka.zasygnalizujPrzegrana();
            Pair wsp_prezentu = siatka.gdzieObokJestPrezent(w, k);
            if(wsp_prezentu != null){ //dziecko budzi się koło prezentu
                int w_prez = wsp_prezentu.w;
                int k_prez = wsp_prezentu.k;
                idzDoPrezentu(w_prez, k_prez);
                czy_ma_prezent = true;
            }
            if (!czy_ma_prezent){
                idzDoMikolajaJesliBlisko();
                long poczatek = System.currentTimeMillis();
                int i = 0;
                int czas_lazenia = losujCzasLazenia();
                while ( !czy_ma_prezent && siatka.czyGraAktywna() && ((System.currentTimeMillis() - poczatek) < czas_lazenia)) {
                    int nr_przes = losowacz.nextInt(4);
                    przesunSie(w_przesun[nr_przes], k_przesun[nr_przes]);
                }
                idzSpac(losujCzasSnu());
                czy_spi = false;
            }
        }
        // dzieciak dostał prezent, trzeba sprawdzić, czy nie jest ostatnim
        synchronized(siatka) {
            siatka.zliczDostarczonyPrezent();
            siatka.sprawdzCzyWygrana();
        }
    }
    private void idzSpac(int czas){
        //jak to uatomowić?
        synchronized(this) {
            czy_spi = true;
            try {
                sleep(czas);
            }
            catch (Exception e) {}

        }

    }

    private int losujCzasLazenia(){ //2 osobne funkcje żeby sterować trudnością
        int[] czasy= {4000, 5000, 6000, 7000};
        int nr = losowacz.nextInt(4);
        return czasy[nr];
    }
    private int losujCzasSnu(){
        int[] czasy= {4000, 5000, 6000, 7000};
        int nr = losowacz.nextInt(4);
        return czasy[nr];
    }
    public boolean czyAktywny(){
        return !(czy_ma_prezent || czy_spi);
    }


}
