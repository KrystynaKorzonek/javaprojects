package pliki;

import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionAdapter;
import java.io.*;

import static javax.swing.JOptionPane.QUESTION_MESSAGE;


public class ListaKatalogu extends JList {
    private File aktualny_katalog;
    private OknoManagera okno_rodzic;
    private File wybrany_plik;
    private int wybrany_indeks;

    public ListaKatalogu(File katalog, OknoManagera okno_rodzic){
        this.okno_rodzic = okno_rodzic;
        ustawKatalog(katalog);
        addListSelectionListener(new ListSelectionListener() {
            @Override
            public void valueChanged(ListSelectionEvent lse) {
                if ((File)getSelectedValue() != null) {
                    wybrany_plik = (File) getSelectedValue();
                    wybrany_indeks = getSelectedIndex();
                }
                else
                    wybrany_plik = null;
            }
        });
        addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent evt) {
                if (evt.getClickCount() == 2) {
                    int index = locationToIndex(evt.getPoint());
                    File f = (File) getModel().getElementAt(index);
                    if (f.isDirectory())
                        ustawKatalog(f);
                }
            }
        });

        addMouseMotionListener(new MouseMotionAdapter() {
            public void mouseMoved(MouseEvent e) {
                int index = locationToIndex(e.getPoint());
                File f = (File) getModel().getElementAt(index);
                if(!f.isDirectory()) {
                    double rozmiar = (double) f.length() / 1000;
                    setToolTipText((int)rozmiar + " KB");
                }
                else{
                    setToolTipText(null);
                }
            }
        });

    }
    public File getWybranyPlik(){
        return wybrany_plik;
    }
    public void ustawKatalog(File katalog){
        //podmiana modeli czy fireContentChange ???
        setModel(new ModelKatalogu(katalog, this));
        aktualny_katalog = katalog;
        setVisibleRowCount(getModel().getSize());
        boolean czy_nadrzedny = (katalog.getParentFile() == null);
        setCellRenderer(new Malarz(czy_nadrzedny));
        okno_rodzic.ustawNazweKatalogu(aktualny_katalog.getAbsolutePath());
    }
    public void zmianaNazwy(){
        if (wybrany_plik != null && !wybrany_plik.isDirectory()){
            String nowe_imie = JOptionPane.showInputDialog("wprowadź nową nazwę dla pliku: " + wybrany_plik.getName());
            if (nowe_imie != null){
                ((ModelKatalogu)getModel()).zmienNazwe(wybrany_indeks, nowe_imie);
            }
        }
    }
    public void usuwanie(){
        if (wybrany_plik != null && !wybrany_plik.isDirectory()){
            String[] opcje = {"tak", "nie"};
            int decyzja = JOptionPane.showOptionDialog(okno_rodzic,
                    "Czy na pewno chcesz usunąć plik " + wybrany_plik.getName() +"?",
                    "Usuwanie pliku",
                    2,
                    QUESTION_MESSAGE,
                    null,
                    opcje,
                    opcje[1]);
            if (decyzja == JOptionPane.YES_OPTION){
                ((ModelKatalogu)getModel()).usun(wybrany_indeks);
                wybrany_plik = null;
            }
        }
    }
    //kopiowanie dzieje się "na zewnątrz" modelu katalogu


}