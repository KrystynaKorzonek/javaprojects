package pliki;

import javax.swing.*;
import java.awt.*;
import java.io.File;
import java.text.SimpleDateFormat;

public class Malarz extends JPanel implements ListCellRenderer {
    private JLabel nazwa_pliku;
    private JLabel czas_mod;
    private boolean czy_nadrzedny;
    private static SimpleDateFormat formatnik = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
    public Malarz(boolean czy_nadrzedny){
        this.czy_nadrzedny = czy_nadrzedny;
        nazwa_pliku = new JLabel();
        czas_mod = new JLabel();
        setLayout(new BorderLayout());
        add(nazwa_pliku, BorderLayout.LINE_START);
        add(czas_mod, BorderLayout.LINE_END);
        czas_mod.setBorder(BorderFactory.createEmptyBorder(0, 10, 0, 10));
    }
    public Component getListCellRendererComponent(JList jList, Object o, int nr, boolean czy_zazn, boolean czy_focus) {
        File f = (File)o;
        if (nr != 0 || czy_nadrzedny) {
            nazwa_pliku.setText(f.getName());
            czas_mod.setText(formatnik.format(f.lastModified()));
        }
        else {
            nazwa_pliku.setText("..");
            czas_mod.setText("");
        }
        Font czcionka_czas = czas_mod.getFont();
        czas_mod.setFont(czcionka_czas.deriveFont(czcionka_czas.getStyle() & ~Font.BOLD));
        if (!f.isDirectory()) {
            double rozmiar = (double) f.length() / 1000;
            nazwa_pliku.setToolTipText(rozmiar + " KB");
        }
        Font czcionka = nazwa_pliku.getFont();
        if (f.isDirectory()) {
            nazwa_pliku.setFont(czcionka.deriveFont(czcionka.getStyle() | Font.BOLD));
        }
        else {
            nazwa_pliku.setFont(czcionka.deriveFont(czcionka.getStyle() & ~Font.BOLD));
        }
        if(czy_zazn) {
            setOpaque(true);
            setBackground(Color.YELLOW);
            repaint();
        }
        else
            setOpaque(false);
        return this;
    }

}
