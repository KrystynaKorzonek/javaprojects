package kalendarz;

import javax.swing.*;

public class ZakladkaMiesiaca extends JPanel {
    private int rok;
    private int miesiac;
    private WidokMiesiaca akt_msc;
    private WidokMiesiaca pop_msc;
    private WidokMiesiaca nast_msc;

    public ZakladkaMiesiaca(int rok, int msc){
        this.rok = rok;
        miesiac = msc;
        inicjalizuj();
        setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
        add(pop_msc);
        add(akt_msc);
        add(nast_msc);
    }
    private void inicjalizuj(){
        int pop_msc_nr = miesiac-1;
        int nast_msc_nr = miesiac+1;
        int nast_rok_nr = rok; //UWAGA, to nie następny rok tylko rok dla następnego miesiąca!!!
        int pop_rok_nr = rok;
        if(miesiac==12){
            nast_msc_nr = 1;
            nast_rok_nr = rok+1;
        }
        if(miesiac==1){
            pop_msc_nr = 12;
            pop_rok_nr = rok-1;
        }
        akt_msc = new WidokMiesiaca(rok, miesiac);
        pop_msc = new WidokMiesiaca(pop_rok_nr, pop_msc_nr);
        nast_msc = new WidokMiesiaca(nast_rok_nr, nast_msc_nr);
    }
    public void ustawMiesiac(int rok, int msc){
        this.rok = rok;
        miesiac = msc;
        remove(pop_msc);
        remove(akt_msc);
        remove(nast_msc);
        inicjalizuj();
        add(pop_msc);
        add(akt_msc);
        add(nast_msc);

    }

}
