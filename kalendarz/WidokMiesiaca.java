package kalendarz;

import javax.swing.*;
import java.awt.*;

class ListaMiesiaca extends JList {
    public ListaMiesiaca(ModelMiesiaca model){
        super(model);
        setVisibleRowCount(model.ileWierszy());
        setLayoutOrientation(JList.HORIZONTAL_WRAP);
    }
}

public class WidokMiesiaca extends JPanel{
    private int rok;
    private int miesiac;
    private ListaMiesiaca lista;
    static String[] nazwy_miesiecy =
            {"", "styczeń", "luty", "marzec", "kwiecień", "maj", "czerwiec", "lipiec",
                    "sierpień", "wrzesień", "październik", "listopad", "grudzień"};
    public WidokMiesiaca(int rok, int msc){
        this.rok = rok;
        miesiac = msc;
        setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
        JLabel nazwa = new JLabel(nazwy_miesiecy[msc]);
        nazwa.setAlignmentX(Component.CENTER_ALIGNMENT);
        lista = new ListaMiesiaca(new ModelMiesiaca(rok, msc));
        lista.setCellRenderer(new Malowacz());
        int szer_listy = lista.getPreferredSize().width;
        nazwa.setPreferredSize(new Dimension(szer_listy, 20));
        add(nazwa);
        add(lista);
    }
    public void zmienRok(int rok){
        ((ModelMiesiaca)lista.getModel()).ustawRok(rok);
    }
    public void ustawLiczbęWierszy(){
        lista.setVisibleRowCount(((ModelMiesiaca)lista.getModel()).ileWierszy());
    }
    public int getRok(){
        return rok;
    }
    public int getMsc(){
        return miesiac;
    }
    public String info(){
        return rok + " " + miesiac;
    }
}

class Malowacz extends JLabel implements ListCellRenderer{
    @Override
    public Component getListCellRendererComponent(JList jList, Object o, int nr, boolean czy_zazn, boolean czy_focus) {
        setText(o.toString());
        Dzien d = (Dzien)o;
        if(d.czyNiedziela())
            setForeground(Color.RED);
        else
            setForeground(Color.BLACK);
        return this;
    }
}