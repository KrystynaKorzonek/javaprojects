package kalendarz;

import javax.swing.*;
import javax.swing.event.*;
import java.awt.*;
import java.awt.event.*;

//PROBLEM: powiększające się menu
//GridBagConstraints + odpowiedni constraints ??

class Menu extends JToolBar{
    private JSpinner wybieracz_roku;
    private SpinnerModel model_wybieracza;
    private JButton guzik_pop_msc, guzik_nast_msc;
    public Menu(int rok) {
        super();
        model_wybieracza = new SpinnerNumberModel(rok, 1900, 2100, 1);
        wybieracz_roku = new JSpinner(model_wybieracza);
        JSpinner.NumberEditor edytor = new JSpinner.NumberEditor(wybieracz_roku, "#");
        wybieracz_roku.setEditor(edytor);
        //wybieracz_roku.setPreferredSize(new Dimension(wybieracz_roku.getPreferredSize().width, 80));
        wybieracz_roku.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {
                ZakladkaRoku zr = (ZakladkaRoku)getParent();
                Integer nowy_rok = (Integer) wybieracz_roku.getValue();
                zr.wyswietlRok(nowy_rok);
                WnetrzeOkna wo = (WnetrzeOkna)zr.getParent();
                wo.ustawZakladkeMiesiaca(nowy_rok, 1);
            }
        });
        add(new JLabel("rok: "));
        add(wybieracz_roku);
        add(new JLabel("miesiąc: "));
        guzik_pop_msc = new JButton(" << ");
        guzik_pop_msc.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                ZakladkaRoku zr = (ZakladkaRoku)getParent();
                zr.miesiacWstecz();
            }
        });
        guzik_nast_msc = new JButton(" >> ");
        guzik_nast_msc.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                ZakladkaRoku zr = (ZakladkaRoku)getParent();
                zr.miesiacNaprzod();
            }
        });
        add(guzik_pop_msc);
        add(guzik_nast_msc);
    }
}

public class ZakladkaRoku extends JPanel {
    private int akt_rok;
    private ListaRoku lista_roku;
    private Menu menu;
    public ZakladkaRoku(int rok){
        akt_rok = rok;
        setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
        lista_roku = new ListaRoku(rok);
        menu = new Menu(rok);
        menu.setPreferredSize(new Dimension(400, 30));
        add(lista_roku);
        add(menu);
    }
    public void wyswietlRok(int rok){
        lista_roku.clearSelection();   // ???
        ((ModelRoku)lista_roku.getModel()).setRok(rok); // funkcją ListyRoku
        ((JTabbedPane)getParent()).setTitleAt(0, Integer.toString(rok));
    }
    public OknoKalendarza getOkno(){
        return (OknoKalendarza)getParent();
    }
    public void miesiacWstecz(){
        WidokMiesiaca model_akt_msc  = (WidokMiesiaca) lista_roku.getSelectedValue();
        if(model_akt_msc == null)
            return;
        int akt_msc = model_akt_msc.getMsc();
        WnetrzeOkna wo = (WnetrzeOkna)getParent();
        if(akt_msc == 1){
            akt_rok--;
            wyswietlRok(akt_rok);
            wo.ustawZakladkeMiesiaca(akt_rok, 12);
            lista_roku.setSelectedIndex(11);
        }
        else{
            wo.ustawZakladkeMiesiaca(akt_rok, akt_msc-1);
            lista_roku.clearSelection();
            lista_roku.setSelectedIndex(akt_msc-2);
        }
    }
    public void miesiacNaprzod(){
        WidokMiesiaca model_akt_msc  = (WidokMiesiaca) lista_roku.getSelectedValue();
        if(model_akt_msc == null)
            return;
        int akt_msc = model_akt_msc.getMsc();
        WnetrzeOkna wo = (WnetrzeOkna)getParent();
        if(akt_msc == 12){
            akt_rok++;
            wyswietlRok(akt_rok);
            wo.ustawZakladkeMiesiaca(akt_rok, 1);
            lista_roku.setSelectedIndex(0);
        }
        else{
            wo.ustawZakladkeMiesiaca(akt_rok, akt_msc+1);
            lista_roku.clearSelection();
            lista_roku.setSelectedIndex(akt_msc+1-1);
        }
    }
}