package kalendarz;

public class Dzien {
    private boolean czy_niedziela;
    private String nr_dnia;
    public Dzien(boolean czy_niedziela, String nr_dnia){
        this.czy_niedziela = czy_niedziela;
        this.nr_dnia = nr_dnia;
    }
    public String toString(){
        return nr_dnia;
    }
    public boolean czyNiedziela(){
        return czy_niedziela;
    }
}
