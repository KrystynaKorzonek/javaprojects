package kalendarz;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.Calendar;


class WnetrzeOkna extends JTabbedPane{
    static String[] nazwy_miesiecy =
            {"", "styczeń", "luty", "marzec", "kwiecień", "maj", "czerwiec", "lipiec",
                    "sierpień", "wrzesień", "październik", "listopad", "grudzień"};

    ZakladkaRoku zakladka_roku;
    ZakladkaMiesiaca zakladka_miesiaca;
    public WnetrzeOkna() {
        Calendar c = Calendar.getInstance();
        int rok = c.get(Calendar.YEAR);
        int msc = c.get(Calendar.MONTH) + 1;
        zakladka_roku = new ZakladkaRoku(rok);
        zakladka_miesiaca = new ZakladkaMiesiaca(rok, msc);
        addTab("", zakladka_roku);
        addTab("", zakladka_miesiaca);
        ustawTytulRok(rok);
        ustawTytulMiesiac(msc);
    }
    public void ustawTytulRok(int rok){
        setTitleAt(0, Integer.toString(rok));
    }
    public void ustawTytulMiesiac(int msc){
        setTitleAt(1, nazwy_miesiecy[msc]);
    }
    public void ustawZakladkeMiesiaca(int rok, int msc){
        zakladka_miesiaca.ustawMiesiac(rok, msc);
        ustawTytulMiesiac(msc);
    }
}

public class OknoKalendarza extends JFrame {
    WnetrzeOkna tabbed_pane;
    public OknoKalendarza() {
        super("Kalendarz");
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setVisible(true);
        setSize(400, 600);
        setPreferredSize(new Dimension(400, 600));
        setResizable(true);
        tabbed_pane = new WnetrzeOkna();
        add(tabbed_pane);
        ListaMiesiaca nowa = new ListaMiesiaca(new ModelMiesiaca(2020, 2));
        nowa.setCellRenderer(new Malowacz());
    }
}
