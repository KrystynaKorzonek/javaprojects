# JavaProjects

Projekty z Kursu Javy na 5. semestrze studiów

Opis wybranych projektów:

## Labirynt
Po każdym uruchumieniu gry losowo generuje się labirynt (w sensie teoriografowym jest to drzewo -- pola labiryntu są wierzchołkami, a przejścia między nimi krawędziami -- dzięki czemu rozwiązanie zawsze istnieje, po całym labiryncie można chodzić i nie jest on za prosty):

Ludzikiem sterujemy za pomoca strzałek. Celem jest zdobycie skarbu.

![obrazek labiryntu](labirynt/przykladowa_gra_labirynt.png)

## Samotnik

Klasyczna jednoosobowa gra logiczna. Żeby zbić pionek wystarczy przeskoczyć nad nim innym pionkiem. Celem jest zbicie wszystkich pionków poza jednym. 

Można grać za pomocą myszki lub klawiatury. Do wyboru są dwie różne plansze (brytyjska i europejska), można też dostosować szatę kolorystyczną.
Kiedy nie można już wykonać żadnego ruchu, program automatycznie ogłosi wygraną lub przegraną. 

![obrazek samotnik](samotnik/przykladowa_gra_samotnik.png)

## Kalendarz

![obrazek z kalendarzem](kalendarz/przykladowy_kalendarz.png)

## Manager plików

Prosty manager plików pozwalający na przeglądanie, kopiowanie, usuwanie i zmienianie nazwy plików. 

