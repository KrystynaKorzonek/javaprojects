package labirynt;

class Pair{
    public int first;
    public int second;
    public Pair(int f, int s){
        first = f;
        second = s;
    }
}