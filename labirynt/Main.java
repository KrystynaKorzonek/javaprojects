package labirynt;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.Random;

import static java.awt.Image.SCALE_SMOOTH;
import static java.lang.Math.floor;

class Pole extends JPanel {
    private Image badacz_obraz;
    protected boolean czy_badacz = false;
    public Pole(Sasiedztwo s, int rozmiar, int kraw) throws Exception{
        badacz_obraz = ImageIO.read(getClass().getResource("ludzik.jpg"));
        badacz_obraz = badacz_obraz.getScaledInstance(rozmiar, rozmiar, SCALE_SMOOTH);
        int grub_gora=0, grub_lewo=0, grub_dol=0, grub_prawo=0;
        if(!s.gora)
            grub_gora = kraw;
        if(!s.lewo)
            grub_lewo = kraw;
        if(!s.prawo)
            grub_prawo = kraw;
        if(!s.dol)
            grub_dol = kraw;
        setBorder(BorderFactory.createMatteBorder(grub_gora, grub_lewo, grub_dol, grub_prawo, Color.red));
    }
    public void ustawBadacza(){
        czy_badacz = true;
        repaint();
    }
    public void zabierzBadacza(){
        czy_badacz = false;
        repaint();
    }
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        if (badacz_obraz != null && czy_badacz) {
           g.drawImage(badacz_obraz, 0, 0, this);
        }
    }
}

class PoleSkarbu extends Pole{
    private Image skarb_obraz;
    public PoleSkarbu(Sasiedztwo s, int rozmiar, int kraw) throws Exception{
        super(s, rozmiar, kraw);
        skarb_obraz = ImageIO.read(getClass().getResource("skarb.jpg"));
        skarb_obraz = skarb_obraz.getScaledInstance(rozmiar, rozmiar, SCALE_SMOOTH);
        czy_badacz = false;
    }
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        if (skarb_obraz != null) {
            g.drawImage(skarb_obraz, 0, 0, this);
        }
    }
}


class OknoGry extends JFrame {
    private static int SZER_OBR = 80;
    private static int SZER_SCIAN = 5;
    private static int ILE_KOM = 12;

    private Graf graf_gry;
    Pole[][] pola = new Pole[ILE_KOM][ILE_KOM];
    private int tu_bad_w;
    private int tu_bad_k;

    private class SluchaczOkienkowy extends WindowAdapter
    {
        @Override
        public void windowClosing (WindowEvent ev){
            OknoGry.this.dispose();
        }
    };

    private void ustawBadacza(int i, int j){
        pola[tu_bad_w][tu_bad_k].zabierzBadacza();
        pola[i][j].ustawBadacza();
        tu_bad_w = i;
        tu_bad_k = j;
    }
    private void przesunBadacza(int poz, int pion){
        int nowy_w = tu_bad_w + pion;
        int nowy_k = tu_bad_k + poz;
        if (0<=nowy_w && nowy_w<ILE_KOM && 0<=nowy_k && nowy_k<ILE_KOM)
            if (graf_gry.czyJestPrzejscie(tu_bad_w, tu_bad_k, nowy_w, nowy_k))
                ustawBadacza(nowy_w, nowy_k);
    }
    private Pair losujWspBadacza(){
        Random r = new Random(System.currentTimeMillis());
        int przes = (int)floor(ILE_KOM*3/4);
        //przynajmniej jedna ze współrzędnych ma być >= przes
        //wtedy wykluczamy punkty startowe z lewego górnego kwadratu przes x przes
        int w, k;
        do {
            w = r.nextInt(ILE_KOM);
            k = r.nextInt(ILE_KOM);
        } while (w<przes && k<przes);
        return new Pair(w, k);
    }
    public OknoGry() throws Exception
    {
        super("Labirynt");
        int szer = ILE_KOM*(SZER_OBR+SZER_SCIAN);
        setSize(szer,szer);
        setLocation(0,0);
        setLayout(new GridLayout(ILE_KOM, ILE_KOM));
        graf_gry = new Graf(ILE_KOM);
        for(int i=0; i<ILE_KOM; i++)
            for(int j=0; j<ILE_KOM; j++) {
                if(i==0 && j==0)
                    pola[0][0] = new PoleSkarbu(graf_gry.sasiedziPola(0,0), SZER_OBR, SZER_SCIAN);
                else
                    pola[i][j] = new Pole(graf_gry.sasiedziPola(i, j), SZER_OBR, SZER_SCIAN);
                add(pola[i][j]);
            }
        Pair wsp_pocz_bad = losujWspBadacza();
        ustawBadacza(wsp_pocz_bad.first, wsp_pocz_bad.second);
        addKeyListener(new KeyListener() {
            @Override
            public void keyTyped(KeyEvent keyEvent) {}
            @Override
            public void keyPressed(KeyEvent e) {
                int kod = e.getKeyCode();
                if (kod == KeyEvent.VK_UP)
                    przesunBadacza(0, -1);
                if (kod == KeyEvent.VK_DOWN)
                    przesunBadacza(0, 1);
                if (kod == KeyEvent.VK_LEFT)
                    przesunBadacza(-1, 0);
                if (kod == KeyEvent.VK_RIGHT)
                    przesunBadacza(1, 0);
            }
            @Override
            public void keyReleased(KeyEvent keyEvent) {}
        });
        addWindowListener(new SluchaczOkienkowy());
        setResizable(false);
        setVisible(true);
    }
}

public class Main {
    public static void main (String[] args) throws Exception {
	    new OknoGry();
    }
}
