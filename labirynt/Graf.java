package labirynt;

import java.util.ArrayDeque;
import java.util.Random;

import static java.lang.Integer.max;
import static java.lang.Integer.min;


public class Graf {
    int ile_kom;
    int ile_kraw;
    private boolean[][] pozioma; //pamiętamy poziome krawędzie - czy z danego pola "wystaje" krawędź w prawo
    private boolean[][] pionowa; //pamiętamy pionowe - czy w dół
    class Kraw{
        //krawędź reprezentowana jako Lewy/Górny wierzchołek(o wsp. (w, k)) i kierunek poziomy/pionowy
        //UWAGA! niektóre kierunki są niedozwolone(prawy/dolny bok planszy); kraw nie może się zaczynać w prawym dolnym rogu
        private int w;
        private int k;
        private boolean czy_poz;
        Kraw(int w, int k, boolean czy_poz){
            this.w = w;
            this.k = k;
            this.czy_poz = czy_poz;
        }
        Pair drugiWierz(){
            if(czy_poz)
                return new Pair(w, k+1);
            else
                return new Pair(w+1, k);
        }
        void dodajDoGrafu(){
            if(czy_poz)
                pozioma[w][k] = true;
            else
                pionowa[w][k] = true;
        }
    }
    private Kraw losujKraw(){
        Random r = new Random(System.currentTimeMillis());
        int w, k;
        do {
            w = r.nextInt(ile_kom);
            k = r.nextInt(ile_kom);
        } while(w==ile_kom-1 && k==ile_kom-1);
        boolean czy_poz = r.nextBoolean();
        if (w == ile_kom-1)
            czy_poz = true;
        if (k == ile_kom-1)
            czy_poz = false;
        return new Kraw(w, k, czy_poz);
    }

    public Graf(int ile_kom){
        pozioma = new boolean[ile_kom][ile_kom];
        pionowa = new boolean[ile_kom][ile_kom];
        this.ile_kom = ile_kom;
        ile_kraw = ile_kom*ile_kom - 1; //graf to drzewo
        int ile_juz_kraw = 0;
        int[][] spoj_sklad = new int[ile_kom][ile_kom];
        int[] ile_w_spoj_sklad = new int[ile_kom*ile_kom];
        int licz = 0;
        for(int i=0; i<ile_kom; i++)
            for(int j=0; j<ile_kom; j++){
                spoj_sklad[i][j] = licz;
                licz++;
            }
        for(int i=0; i<ile_kom*ile_kom; i++)
            ile_w_spoj_sklad[i] = 1;
        while (ile_juz_kraw < ile_kraw){
            Kraw kraw = losujKraw();
            int w2 = kraw.drugiWierz().first;
            int k2 = kraw.drugiWierz().second;
            if(spoj_sklad[kraw.w][kraw.k]!=spoj_sklad[w2][k2]){ //bierzemy tę krawędź
                kraw.dodajDoGrafu();
                ile_juz_kraw++;
                //łączenie spójnych składowych
                int ss1 = spoj_sklad[kraw.w][kraw.k];
                int ss2 = spoj_sklad[w2][k2];
                if (ile_w_spoj_sklad[ss1]<ile_w_spoj_sklad[ss2]) {
                    dolaczDoSpojnejSkladowej(ss2, spoj_sklad, kraw.w, kraw.k);
                    ile_w_spoj_sklad[ss2] += ile_w_spoj_sklad[ss1];
                }
                else {
                    dolaczDoSpojnejSkladowej(ss1, spoj_sklad, w2, k2);
                    ile_w_spoj_sklad[ss1] += ile_w_spoj_sklad[ss2];
                }
            }
        }
    }
    private void dolaczDoSpojnejSkladowej(int nr_nadawanej_sklad, int spoj_sklad[][], int w, int k){ // w i k - współrzedne od których zaczynamy
        ArrayDeque<Pair> kolejka = new ArrayDeque<Pair>();
        int[] delta_w = {1, -1, 0, 0};
        int[] delta_k = {0, 0, 1, -1};
        int nr_zmienianej_sklad = spoj_sklad[w][k];
        kolejka.push(new Pair(w, k));
        while(!kolejka.isEmpty()){
            Pair p = kolejka.pop();
            spoj_sklad[p.first][p.second] = nr_nadawanej_sklad;
            for(int i=0; i<4; i++){
                int nowe_w = p.first + delta_w[i];
                int nowe_k = p.second + delta_k[i];
                if(czyOkWsp(nowe_w, nowe_k))
                    if(spoj_sklad[nowe_w][nowe_k] == nr_zmienianej_sklad)
                        kolejka.push(new Pair(nowe_w, nowe_k));
            }
        }
    }
    private boolean czyOkWsp(int w, int k){
        return (0<=w && w<ile_kom && 0<=k & k<ile_kom);
    }

    public boolean czyJestPrzejscie(int w1, int k1, int w2, int k2){
        if (!czyOkWsp(w1, k1) || !czyOkWsp(w2, k2))
            return false;
        if(w1==w2){ //ten sam wiersz, powinny być różne kolumny
            int k_max = max(k1, k2);
            int k_min = min(k1, k2);
            if (k_max - k_min != 1) //nie są odległe o 1
                return false;
            return pozioma[w1][k_min];
        }
        else{ //różne wiersze, powinna być ta sama kolumna
            if (k1!=k2)
                return false; //nie sąsiadują
            int w_max = max(w1, w2);
            int w_min = min(w1, w2);
            if (w_max - w_min != 1)
                return false;
            return pionowa[w_min][k1];
        }
    }

    public Sasiedztwo sasiedziPola(int w, int k){
        boolean czy_gora = czyJestPrzejscie(w, k, w-1, k);
        boolean czy_dol = czyJestPrzejscie(w, k, w+1, k);
        boolean czy_lewo = czyJestPrzejscie(w, k, w, k-1);
        boolean czy_prawo = czyJestPrzejscie(w, k, w, k+1);
        return new Sasiedztwo(czy_gora, czy_dol, czy_lewo, czy_prawo);
    }
}
