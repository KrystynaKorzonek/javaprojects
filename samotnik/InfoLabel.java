package samotnik;

import javax.swing.*;
import java.awt.*;

public class InfoLabel extends JLabel {
    public InfoLabel(int szer){
        setSize(szer, 100);
        setPreferredSize(new Dimension(szer, 100));
        setFont(new Font("", 0, 30));
        setHorizontalAlignment(SwingConstants.CENTER);
        setVerticalAlignment(SwingConstants.CENTER);
        setVisible(true);
    }
    public void ustawInfoIle(int ile_pionow){
        setText("       ZOSTAŁO " + ile_pionow + " PIONÓW       ");
    }
    public void ustawInfoWygrana(){
        setText("ZWYCIĘŚTWO!");
    }
    public void ustawInfoPrzegrana(int ile_pionów){
        setText("<html>PRZEGRANA!<br> ZOSTAŁO: " + ile_pionów + " PIONÓW</html>");
    }
}
