package samotnik;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

/*
KOMUNIKACJA:
--> pole wysyła sygnał do rodzica, fkcją publiczną rodzica? źle, że publiczna
zaznaczenie z Pola, idzie do OknaGry, a potem oknoGry przestawia pionek... to jakoś nie po kolei
--> ogólny słuchacz na całe pole+obliczanie wymiarów - brzydkie implementacyjnie i ryzykowne
 */

public class OknoGry extends JFrame {
    private InterfaceGry akt_gra;
    private JMenuBar menu;
    private int SZER = (80+2)*7+2*6;
    private boolean czy_europ = false;

    public OknoGry() throws Exception{ //tu jako argument typplanszy
        super("Samotnik");
        akt_gra = new InterfaceGry(czy_europ);
        menu = new JMenuBar();
        zbudujPasekMenu();
        getContentPane().setLayout(new BoxLayout(getContentPane(), BoxLayout.Y_AXIS));
        add(akt_gra);
        setJMenuBar(menu);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        pack();
        setResizable(true); //tu true
        setVisible(true);
    }
    private void zbudujPasekMenu() throws Exception{
        JMenu menu_gra = new JMenu("gra");
        JMenu menu_ruchy = new JMenu("ruchy");
        JMenu menu_ust = new JMenu("ustawienia");
        JMenu menu_pomoc = new JMenu("pomoc");
        //GRA
        JMenuItem nowa_gra = new JMenuItem(new AbstractAction("nowa gra") {
            public void actionPerformed (ActionEvent e) {
                try{
                    remove(akt_gra);
                    akt_gra = new InterfaceGry(czy_europ);
                    akt_gra.repaint();
                    add(akt_gra);
                    pack();
                    repaint();
                }
                catch (Exception ex){}
            }
        });
        JMenuItem koniec = new JMenuItem(new AbstractAction("koniec") {
            public void actionPerformed(ActionEvent e) {
                dispose();
            }
        });
        nowa_gra.setMnemonic(KeyEvent.VK_N);
        koniec.setMnemonic(KeyEvent.VK_K);
        menu_gra.add(nowa_gra);
        menu_gra.add(koniec);

        //RUCHY
        JMenuItem podswietl_srodek = new JMenuItem(new AbstractAction("podświetl środek") {
            public void actionPerformed(ActionEvent e) {
                akt_gra.ustawFocusSrodek();
            }
        });
        JMenuItem gora = new JMenuItem(new AbstractAction("w górę!") {
            public void actionPerformed(ActionEvent e) {
                akt_gra.przesunFocus(-1, 0);
            }
        });
        JMenuItem dol = new JMenuItem(new AbstractAction("w dół!") {
            public void actionPerformed(ActionEvent e) {
                akt_gra.przesunFocus(1, 0);
            }
        });
        JMenuItem lewo = new JMenuItem(new AbstractAction("w lewo!") {
            public void actionPerformed(ActionEvent e) {
                akt_gra.przesunFocus(0, -1);
            }
        });
        JMenuItem prawo = new JMenuItem(new AbstractAction("w prawo!") {
            public void actionPerformed(ActionEvent e) {
                akt_gra.przesunFocus(0, 1);
            }
        });
        JMenuItem bij_gora = new JMenuItem(new AbstractAction("bij w górę") {
            public void actionPerformed(ActionEvent e) {
                akt_gra.nacisnietoPoleDelta(-2, 0);
            }
        });
        JMenuItem bij_dol = new JMenuItem(new AbstractAction("bij w dół") {
            public void actionPerformed(ActionEvent e) {
                akt_gra.nacisnietoPoleDelta(2, 0);
            }
        });
        JMenuItem bij_lewo = new JMenuItem(new AbstractAction("bij w lewo") {
            public void actionPerformed(ActionEvent e) {
                akt_gra.nacisnietoPoleDelta(0, -2);
            }
        });
        JMenuItem bij_prawo = new JMenuItem(new AbstractAction("bij w prawo") {
            public void actionPerformed(ActionEvent e) {
                akt_gra.nacisnietoPoleDelta(0, 2);
            }
        });

        gora.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_UP, ActionEvent.CTRL_MASK));
        dol.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_DOWN, ActionEvent.CTRL_MASK));
        lewo.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_LEFT, ActionEvent.CTRL_MASK));
        prawo.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_RIGHT, ActionEvent.CTRL_MASK));

        bij_gora.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_UP, ActionEvent.SHIFT_MASK));
        bij_dol.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_DOWN, ActionEvent.SHIFT_MASK));
        bij_lewo.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_LEFT, ActionEvent.SHIFT_MASK));
        bij_prawo.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_RIGHT, ActionEvent.SHIFT_MASK));

        podswietl_srodek.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_S, ActionEvent.CTRL_MASK));

        menu_ruchy.add(gora);
        menu_ruchy.add(dol);
        menu_ruchy.add(lewo);
        menu_ruchy.add(prawo);
        menu_ruchy.add(bij_gora);
        menu_ruchy.add(bij_dol);
        menu_ruchy.add(bij_lewo);
        menu_ruchy.add(bij_prawo);
        menu_ruchy.add(podswietl_srodek);

        //USTAWIENIA
        JMenu kolor_planszy = new JMenu("kolor planszy");
        JMenu kolor_pionkow = new JMenu("kolor pionkow");
        JMenuItem czerwony_plansza = new JMenuItem(new AbstractAction("czerwony") {
            public void actionPerformed(ActionEvent e) {
                akt_gra.zmienKolorPlanszy(Color.RED);
            }
        });
        JMenuItem niebieski_plansza = new JMenuItem(new AbstractAction("niebieski") {
            public void actionPerformed(ActionEvent e) {
                akt_gra.zmienKolorPlanszy(Color.BLUE);
            }
        });
        JMenuItem zielony_plansza = new JMenuItem(new AbstractAction("zielony") {
            public void actionPerformed(ActionEvent e) {
                akt_gra.zmienKolorPlanszy(Color.GREEN);
            }
        });
        JMenuItem czarny_pionek = new JMenuItem(new AbstractAction("czarny") {
            public void actionPerformed(ActionEvent e) {
                akt_gra.zmienKolorPionkow(Color.BLACK);
            }
        });
        JMenuItem czerwony_pionek = new JMenuItem(new AbstractAction("czerwony") {
            public void actionPerformed(ActionEvent e) {
                akt_gra.zmienKolorPionkow(Color.RED);
            }
        });
        JMenuItem niebieski_pionek = new JMenuItem(new AbstractAction("niebieski") {
            public void actionPerformed(ActionEvent e) {
                akt_gra.zmienKolorPionkow(Color.BLUE);
            }
        });
        JMenuItem zielony_pionek = new JMenuItem(new AbstractAction("zielony") {
            public void actionPerformed(ActionEvent e) {
                akt_gra.zmienKolorPionkow(Color.GREEN);
            }
        });
        JRadioButtonMenuItem europejska = new JRadioButtonMenuItem(new AbstractAction("europejska") {
            public void actionPerformed(ActionEvent e) {
                czy_europ = true;
            }
        });
        JRadioButtonMenuItem brytyjska = new JRadioButtonMenuItem(new AbstractAction("brytyjska") {
            public void actionPerformed(ActionEvent e) {
                czy_europ = false;
            }
        });

        kolor_planszy.add(czerwony_plansza);
        kolor_planszy.add(niebieski_plansza);
        kolor_planszy.add(zielony_plansza);

        kolor_pionkow.add(czarny_pionek);
        kolor_pionkow.add(czerwony_pionek);
        kolor_pionkow.add(niebieski_pionek);
        kolor_pionkow.add(zielony_pionek);

        ButtonGroup grupa_rodzaj_planszy = new ButtonGroup();
        grupa_rodzaj_planszy.add(brytyjska);
        grupa_rodzaj_planszy.add(europejska);


        JMenu rodzaj_planszy = new JMenu("rodzaj planszy");
        rodzaj_planszy.add(brytyjska);
        rodzaj_planszy.add(europejska);

        menu_ust.add(kolor_planszy);
        menu_ust.add(kolor_pionkow);
        menu_ust.add(rodzaj_planszy);

        //POMOC
        String tekst_o_grze = "Celem gry jest zbicie wszystkich pionków poza jednym\n" +
                "Pionek można zbić przeskakując nad nim sąsiadującym z nim pionkiem.\n" +
                "Można bić w jednym z czterech kierunków: do góry, w doł, w lewo lub w prawo\n" +
                "Pole za zbijanym pionkiem musi być wolne";
        String tekst_o_aplikacji = "Autor: Krystyna Korzonek\n" +
                "data utworzenia: 02.12.2020";
        JMenuItem o_grze = new JMenuItem(new AbstractAction("o grze") {
            public void actionPerformed(ActionEvent e) {
                JOptionPane.showMessageDialog(null,
                        tekst_o_grze, "O GRZE", JOptionPane.PLAIN_MESSAGE);
            }
        });
        JMenuItem o_aplikacji = new JMenuItem(new AbstractAction("o aplikacji") {
            public void actionPerformed(ActionEvent e) {
                JOptionPane.showMessageDialog(null,
                        tekst_o_aplikacji, "O APLIKACJI", JOptionPane.PLAIN_MESSAGE);
            }
        });
        menu_pomoc.add(o_grze);
        menu_pomoc.add(o_aplikacji);

        menu.add(menu_gra);
        menu.add(menu_ruchy);
        menu.add(menu_ust);
        menu.add(menu_pomoc);

    }
}








