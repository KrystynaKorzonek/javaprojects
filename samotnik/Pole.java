package samotnik;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;

class Pole extends JPanel {
    private boolean czy_pionek = false;
    private boolean czy_zazn = false;
    private int w, k;
    private int grub_gora=1, grub_lewo=1, grub_dol=1, grub_prawo=1;
    private static Color kolor_planszy;
    private static Color kolor_pionka;
    public static void setKolorPionka(Color c){
        kolor_pionka = c;
    }
    public Pole(){}
    public Pole(Sasiedztwo s, int w, int k, boolean czy_pionek, Color kol) throws Exception{
        //s dostarcza informacji o granicach
        this.czy_pionek = czy_pionek;
        this.w = w;
        this.k = k;
        int kraw = 6;
        if(s.gora)
            grub_gora = kraw;
        if(s.lewo)
            grub_lewo = kraw;
        if(s.prawo)
            grub_prawo = kraw;
        if(s.dol)
            grub_dol = kraw;

        setBorder(BorderFactory.createMatteBorder(grub_gora, grub_lewo, grub_dol, grub_prawo, kol));
        kolor_planszy = jasniejszyKolor(kol);
        setBackground(kolor_planszy);
        addMouseListener(new SluchaczMyszkowy());
    }
    public void ustawKolor(Color c){
        setBorder(BorderFactory.createMatteBorder(grub_gora, grub_lewo, grub_dol, grub_prawo, c));
        kolor_planszy = jasniejszyKolor(c);
        setBackground(kolor_planszy);
        repaint();
    }
    public void ustawPionek(){
        czy_pionek = true;
        repaint();
    }
    public void zabierzPionek(){
        czy_pionek = false;
        repaint();
    }
    public void wolajLogikeZPlanszy(){
        Plansza pl = (Plansza)getParent();
        InterfaceGry ig = (InterfaceGry)pl.getParent();
        ig.nacisnietoPole(w, k);
    }
    public void zmienZaznaczenie(){
        if(czy_zazn)
            setBackground(kolor_planszy);
        else
            setBackground(Color.yellow);
        czy_zazn = !czy_zazn;
        repaint();

    }
    public void ustawKolorPionka(){
        repaint();
    }
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        if (czy_pionek) {
            int r= getWidth()/2;
            int wsp = getWidth()/4;
            g.setColor(kolor_pionka);
            g.drawOval(wsp, wsp, r, r);
            g.fillOval(wsp, wsp, r, r);
        }
    }
    static Color jasniejszyKolor(Color c){ //biblioteczne brighter() nie działa //??
        int rozjasniacz = 2;
        int r = c.getRed() + (255-c.getRed())/rozjasniacz;
        int g = c.getGreen() + (255-c.getGreen())/rozjasniacz;
        int b = c.getBlue() + (255-c.getBlue())/rozjasniacz;
        return new Color(r, g, b);
    }
}