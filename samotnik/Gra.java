package samotnik;

import static java.lang.Math.min;
import static java.lang.Math.max;

public class Gra {
    private boolean[][]  czy_pionek;
    private boolean czy_europ; //europejska ma 4 dodatkowe pola
    private int ile_pionkow;

    public Gra(boolean czy_europ){
        this.czy_europ = czy_europ;
        czy_pionek = new boolean[7][7];
        for(int i=0; i<7; i++)
            for(int j=0; j<7; j++)
                czy_pionek[i][j] = true;
        czy_pionek[3][3] = false;
        if(czy_europ)
            ile_pionkow = 36;
        else
            ile_pionkow = 32;
        //ile_ruchow = 4;
    }
    public int getIlePionkow(){return ile_pionkow;}
    public boolean czyJestPionek(int w, int k){
        return czy_pionek[w][k];
    }
    boolean czyPolePlanszy(int w, int k){
        if (w<0 || w>=7 || k<0 || k>=7)
            return false;
        if(czy_europ)
            if((w==1 || w==5) && (k==1 || k==5))
                return true;
        if (w==0 || w==1 || w==5 || w==6)
            if (k==0 || k==1 || k==5 || k==6)
                return false;
        return true;
    }
    Sasiedztwo gdzieGranice(int w, int k){ //zwraca, gdzie dane pole styka się z brzegiem planszy
        //kol. w Sasiedztwie: góra, dół, lewo, prawo
        int[] delta_w = {-1, 1, 0, 0};
        int[] delta_k = {0, 0, -1, 1};
        boolean[] gran = new boolean[4];
        for(int i=0; i<4; i++)
            gran[i] = !czyPolePlanszy(w+delta_w[i], k+delta_k[i]);
        return new Sasiedztwo(gran[0], gran[1], gran[2], gran[3]);
    }

    boolean czyMoznaBic(int w, int k, int w2, int k2){
        if(!czyPolePlanszy(w, k) || !czyPolePlanszy(w2, k2))
            return false;
        if(!czy_pionek[w][k] || czy_pionek[w2][k2])
            return false;
        int k_min = min(k, k2);
        int k_max = max(k, k2);
        int w_min = min(w, w2);
        int w_max = max(w, w2);
        if (!((w==w2 && k_max-k_min==2) || (k==k2 && w_max-w_min==2)))
            return false;
        int w_sr = (w+w2)/2;
        int k_sr = (k+k2)/2;
        //tu wiemy już że to pola, że na jednym jest pionek a na drugim nie, że są odległe o 2
        //do sprawdzenia zostało, czy jest co bić:
        return czy_pionek[w_sr][k_sr];
        /*
        if(w==w2){ //bijemy w poziomie
            int k_min = min(k, k2);
            int k_max = max(k, k2);
            if (k_max-k_min != 2)
                return false;
            //dobre ustawienie, sprawdzamy, czy jest pionek
            return czy_pionek[w][k_min+1];
        }
        if(k==k2){ //bijemy w poziomie
            int w_min = min(w, w2);
            int w_max = max(w, w2);
            if (w_max-w_min != 2)
                return false;
            //dobre ustawienie, sprawdzamy, czy jest pionek
            return czy_pionek[w_min+1][k];
        }
        return false;*/
    }
    void bij(int w, int k, int w2, int k2){
        czy_pionek[w][k] = false;
        czy_pionek[w2][k2] = true;
        int sr_w = (w + w2)/2;
        int sr_k = (k + k2)/2;
        czy_pionek[sr_w][sr_k] = false;
        ile_pionkow--;
    }
    //UWAGA! napisać efektywniej!
    public boolean czyJestJeszczeRuch(){
        int[] delta_w = {-2, 2, 0, 0};
        int[] delta_k = {0, 0, -2, 2};
        for(int w=0; w<7; w++)
            for(int k=0; k<7; k++)
                for(int i=0; i<4; i++)
                    if (czyMoznaBic(w, k, w+delta_w[i], k+delta_k[i]))
                        return true;
        return false;

    }


}
